// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './polyfill';


// Vue + Vue plugins import
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueI18n from 'vue-i18n';
import Vuelidate from 'vuelidate';
import { ClientTable } from 'vue-tables-2';

// other libraries
import axios from 'axios';
import NProgress from 'nprogress';

// internal Files
import App from './App';
import router from './router/index';
import { messages } from './i18n/general.i18n';
import config from '../vue.config';
import store from './store/store';

import './assets/style.css'

Vue.use(BootstrapVue);
Vue.use(VueI18n);
Vue.use(ClientTable, {}, false, 'bootstrap4', 'default');
Vue.use(Vuelidate);





import './utils/filters';
axios.defaults.baseURL = config.baseUrl;
axios.defaults.headers.common['Content-Type'] = 'application/json';
NProgress.configure({ showSpinner: false });
axios.interceptors.request.use(
  conf => {
    NProgress.start();
    conf.headers.common['Authorization'] = 'Bearer ' + store.state.userData.jwt;
    return conf;
  },
  error => {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  response => {
    NProgress.done();
    return response;
  },
  error => {
    NProgress.done();
    return Promise.reject(error);
  }
);


const i18n = new VueI18n({
  locale: 'it-IT',
  messages
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  template: '<App/>',
  components: {
    App
  }
});
