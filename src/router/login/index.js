import LoginPage from '@/containers/Login';

const loginRoute = {
  name: 'Login',
  path: 'login',
  component: LoginPage
};

export default loginRoute;
