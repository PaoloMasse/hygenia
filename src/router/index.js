import Vue from 'vue';
import Router from 'vue-router';

// Routes
import loginRoute from './login';
import mainRoute from './main';

// Setup VueRouter
Vue.use(Router);

const router = new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/main',
      component: {
        render(c) {
          return c('router-view');
        }
      },
      children: [loginRoute, mainRoute],
      meta: {
        breadcrumbHidden: true
      }
    }
  ]
});

export default router;
