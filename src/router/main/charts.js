// Views
import charts from '@/views/Charts';

// Store

const chartsRoute = {
  path: 'charts',
  name: 'Charts',
  meta: {
    breadcrumbHidden: true
  },
  component: charts
};

export default chartsRoute;
