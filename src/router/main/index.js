// Store
import store from '@/store/store';
import dashboardRoute from './dashboard';
import usersRoute from './users';
import smartDispenserRoute from "./smartDispenser";
import smartMopRoute from "./smartMop";
import AssetPeopleRoute from "./assetPeople";
import footfallAnalyticsRoute from "./footfallAnalytics";
import chartsRoute from "./charts";
import masterDetailsRoute from "./masterDetails";

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer');

// Views

const mainRoute = {
  name: 'Main',
  path: 'main',
  redirect: '/main/dashboard',
  meta: {
    breadcrumbHidden: true
  },
  beforeEnter(to, from, next) {
    console.log({ to, from });
    const token = localStorage.getItem('hygeniaToken');
    if (!token) {
      return next('/login');
    }
    next();
  },
  component: DefaultContainer,
  children: [dashboardRoute, usersRoute, smartDispenserRoute, smartMopRoute, AssetPeopleRoute,footfallAnalyticsRoute, chartsRoute, masterDetailsRoute]
};

export default mainRoute;
