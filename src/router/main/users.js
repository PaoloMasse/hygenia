// Views
import Users from '@/views/Users';

// Store

const usersRoute = {
  path: 'users',
  name: 'Users',
  meta: {
    breadcrumbHidden: true
  },
  component: Users
};

export default usersRoute;
