// Views
import SmartMop from '@/views/SmartMop';

// Store

const smartMopRoute = {
  path: 'smart-mop',
  name: 'SmartMop',
  meta: {
    breadcrumbHidden: true
  },
  component: SmartMop
};

export default smartMopRoute;
