// Views
import Dashboard from '@/views/Dashboard';

// Store

const dashboardRoute = {
  path: 'dashboard',
  name: 'Dashboard',
  meta: {
    breadcrumbHidden: true
  },
  component: Dashboard
};

export default dashboardRoute;
