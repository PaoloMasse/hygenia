// Views
import SmartDispenser from '@/views/SmartDispenser';

// Store

const smartDispenserRoute = {
  path: 'smart-dispenser',
  name: 'SmartDispenser',
  meta: {
    breadcrumbHidden: true
  },
  component: SmartDispenser
};

export default smartDispenserRoute;

