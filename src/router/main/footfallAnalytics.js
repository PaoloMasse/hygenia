// Views
import FootfallAnalytics from '@/views/FootfallAnalytics';

// Store

const footfallAnalyticsRoute = {
  path: 'footfall-analytics',
  name: 'FootfallAnalytics',
  meta: {
    breadcrumbHidden: true
  },
  component: FootfallAnalytics
};

export default footfallAnalyticsRoute;
