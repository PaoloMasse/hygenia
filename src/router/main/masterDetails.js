// Views
import masterDetails from '@/views/MasterDetails';

// Store

const masterDetailsRoute = {
    path: 'masterDetails',
    name: 'masterDetails',
    meta: {
        breadcrumbHidden: true
    },
    component: masterDetails,
    props: (route) => ({...route.params })
};

export default masterDetailsRoute;
