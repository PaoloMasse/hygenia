// Views
import AssetPeople from '@/views/AssetPeople';

// Store

const assetPeopleRoute = {
  path: 'asset-people',
  name: 'AssetPeople',
  meta: {
    breadcrumbHidden: true
  },
  component: AssetPeople
};

export default assetPeopleRoute;
