import hmacSHA256 from 'crypto-js/hmac-sha256';

export const getPeriscopeUrl = (data) => {
  const str = encodeURIComponent(JSON.stringify(data));
  const apiKey = '6db34b51-001a-4c9d-b177-c1de4692';
  const path =  '/api/embedded_dashboard?data='+str;
  const signature = hmacSHA256(path, apiKey).toString();
  return `https://www.periscopedata.com/api/embedded_dashboard?data=${str}&signature=${signature}`;
};
