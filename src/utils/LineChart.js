import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Line,
    // mixins: [reactiveProp],
    // props: ['lineChartData', 'options'],
    data: function() {
        return {
            datacollection: {
                labels: [
                    '01/05/2019',
                    '02/05/2019',
                    '03/05/2019',
                    '04/05/2019',
                    '05/05/2019',
                    '06/05/2019',
                    '07/05/2019',
                    '08/05/2019',
                    '09/05/2019',
                    '10/05/2019',
                    '11/05/2019',
                    '12/05/2019',
                    '13/05/2019',
                    '14/05/2019',
                    '15/05/2019',
                    '16/05/2019',
                    '17/05/2019',
                    '18/05/2019',
                    '19/05/2019',
                    '20/05/2019'
                ],
                datasets: [{
                    label: 'Data One',
                    // backgroundColor: '#f87979',
                    pointBackgroundColor: 'white',
                    borderWidth: 1,
                    pointBorderColor: '#249EBF',
                    data: [100, 98, 95, 90, 80, 75, 68, 65, 60, 57, 50, 45, 38, 35, 32, 30, 27, 25, 21, 15]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: true
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return tooltipItems.yLabel;
                        }
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                height: 200,
                pan: {
                    enabled: true,
                    mode: 'xy'
                },
                zoom: {
                    enabled: true,
                    mode: 'xy',
                },
            }
        }
    },
    mounted() {
        // this.chartData is created in the mixin
        this.renderChart(this.datacollection, this.options)
    }
}