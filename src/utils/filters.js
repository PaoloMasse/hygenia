import Vue from "vue"
import moment from 'moment'
import _ from 'lodash';

Vue.filter("capitalize", _.capitalize);
