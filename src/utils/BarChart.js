import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Bar,
    // mixins: [reactiveProp],
    // props: ['barChartData', 'barOptions'],
    data: function() {
        return {
            datacollection: {
                labels: ['Dispenser 001', 'Dispenser 002', 'Dispenser 003', 'Dispenser 004', 'Dispenser 005', 'Dispenser 006', 'Dispenser 007'],
                datasets: [{
                        label: 'Dispenser 001',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [40, 0, 0, 0, 0, 0, 0]
                    },
                    {
                        label: 'Dispenser 002',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [0, 20, 0, 0, 0, 0, 0]
                    },
                    {
                        label: 'Dispenser 003',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [0, 0, 30, 0, 0, 0, 0]
                    },
                    {
                        label: 'Dispenser 004',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [0, 0, 0, 50, 0, 0, 0]
                    },
                    {
                        label: 'Dispenser 005',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [0, 0, 0, 0, 90, 0, 0]
                    },
                    {
                        label: 'Dispenser 006',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [0, 0, 0, 0, 0, 10, 0]
                    },
                    {
                        label: 'Dispenser 007',
                        backgroundColor: '#00cc99',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        data: [0, 0, 0, 0, 0, 0, 20]
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: true
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: {
                    display: true,
                    position: "right"
                },
                tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return tooltipItems.yLabel;
                        }
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                height: 200
            }
        }
    },
    mounted() {
        // this.chartData is created in the mixin
        this.renderChart(this.datacollection, this.options)
    }
}