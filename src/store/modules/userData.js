import axios from 'axios';
import Vue from 'vue'
import _ from 'lodash';

import router from '../../router';

const state = {
  jwt: 'not null',
  details: null
};

const getters = {
  isAuthenticated(state) {
    return state.jwt !== null
  },
  token: function (state) {
    return state.jwt;
  },
  currentUser: function (state) {
    return state.details
  }
};

const mutations = {
  authUser(state, userInfo) {
    Vue.set(state, 'jwt', userInfo.token);
    Vue.set(state, 'details', userInfo.user);
  },
  setCurrentUser: function (state, newUserInfo) {
    Vue.set(state, 'details', newUserInfo);
  },
  clearUserData: function (state) {
    Vue.set(state, 'jwt', null);
    Vue.set(state, 'details', null);
  }
};

const actions = {
  login({ commit }, userData) {
    axios({
      url: '/auth',
      method: 'post',
      headers: {
        Authorization: 'Basic ' + btoa(userData.email + ':' + userData.password)
      }
    })
      .then( res => {
        commit('authUser', { token: res.data.token, user: { ...res.data.user } });
      })
      .then(() => {
        router.push('/');
      })
      .catch( e => {
        Vue.prototype.$notify({
          title: 'Errore',
          text: e.response.statusText,
          type: 'error'
        });
      })
  },
  logout({ rootState, commit }) {
    _.forEach(rootState, (moduleValue, moduleKey) => {
      if (typeof  moduleValue === 'object') {
        _.forEach(moduleValue, (value, key) => {
          const moduleValueType = typeof  value;
          let resetValue;
          switch (moduleValueType) {
            case 'boolean':
              resetValue = false;
              break;
            case 'object':
              resetValue = Array.isArray(value) ? [] : {};
              break;
            case 'string':
            case 'number':
              resetValue = undefined;
              break;
          }
          Vue.set(rootState[moduleKey], key, resetValue)
        })
      }
    });
    commit('clearUserData');
    router.replace('/login');
  },
  updateCurrentUser: async function({ commit }, user) {
    try {
      const { data : updatedUser } = await axios.put(`/users/${user._id}`, user);
      commit('setCurrentUser', updatedUser);
      Vue.prototype.$notify({
        title: 'Successo',
        text: 'Profilo aggiornato con successo!',
        type: 'success'
      })
    }
    catch (e) {
      Vue.prototype.$notify({
        title: 'Errore',
        text: e.response.statusText,
        type: 'error'
      });
      console.log(e);
    }
  }
};

export default {
  state,
  mutations,
  actions,
  getters
}
