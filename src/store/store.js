import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';


// modules
import userData from './modules/userData';

Vue.use(Vuex);

export default new Vuex.Store ({
  modules: {
    userData
  },
  plugins: [createPersistedState({
    key: 'odin',
    paths: ['userData.jwt', 'userData.details']
  })]
})
