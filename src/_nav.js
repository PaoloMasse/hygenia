export default {
    items: [{
            name: 'Dashboard',
            url: 'dashboard',
            icon: 'fa fa-clone'
        },
        {
            name: 'Smart Dispenser',
            url: 'smart-dispenser',
            icon: 'fa fa-lightbulb-o'
        },
        {
            name: 'Dispenser Plus',
            url: 'smart-mop',
            icon: 'fa fa-rss-square'
        },
        {
            name: 'Asset e People Tracking',
            url: 'asset-people',
            icon: 'fa-user-circle'
        },
        {
            name: 'Footfall Analytics',
            url: 'footfall-analytics',
            icon: 'icon-speedometer'
        },
        {
            name: 'Users',
            url: 'users',
            icon: 'fa fa-user'
        }
    ]
}