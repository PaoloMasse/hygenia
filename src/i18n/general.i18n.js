export const messages = {
  'it-IT': {
    cannotBeEmpty: 'campo richiesto',
    invalidEmail: 'email non valida',
    login: 'login',
    signIn: 'registrati'
  },
  'en-EN': {
    cannotBeEmpty: 'required field',
    invalidEmail: 'email not valid',
    login: 'login',
    signIn: 'sign in'
  }
};
