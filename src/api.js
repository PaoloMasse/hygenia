import axios from 'axios'

export const base_url = 'https://simon-e-server-dev.herokuapp.com';
const hygenia_token = localStorage.getItem('hygenia_token');

//set new token
export function setToken(newToken) {
    localStorage.setItem('hygenia_token', newToken);
}

// authentication
export async function authentication() {
    let response = await axios.post(`${base_url}/auth`, {}, {
        auth: {
            username: 'it@hygenia.it',
            password: 'h1g3n14!'
        }
    });
    return response.data
}

// PLANT
export async function retrivePlant(plantId) {
    let response = await axios.get(`${base_url}/plants/${plantId}?access_token=${hygenia_token}`);
    return response.data
}

export async function retrivePlantDatapoints(plantId) {
    let response = await axios.get(`${base_url}/plants/${plantId}/datapoints?access_token=${hygenia_token}`);
    return response.data
}

// datapoints
export async function retriveDatapointsTimeseries(datapointId) {
    let response = await axios.get(`${base_url}/datapoints/${datapointId}/timeseries?access_token=${hygenia_token}`);
    return response.data
}

export async function retriveDatapoint(datapointId) {
    let response = await axios.get(`${base_url}/datapoints/${datapointId}?access_token=${hygenia_token}`);
    return response.data
}

// devices
export async function getDevices(plantId) {
    let response = await axios.get(`${base_url}/devices?access_token=${hygenia_token}&parentId=${plantId}`);
    return response.data;
}

export async function retriveDevice(deviceId) {
    let response = await axios.get(`${base_url}/devices/${deviceId}?access_token=${hygenia_token}`);
    return response.data
}

export async function retriveDeviceDatapoints(deviceId) {
    let response = await axios.get(`${base_url}/devices/${deviceId}/datapoints?access_token=${hygenia_token}`);
    return response.data
}