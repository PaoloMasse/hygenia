module.exports = {
  devServer: {
    proxy: 'http://localhost:4000'
  },
  baseUrl: './',
  lintOnSave: false,
  runtimeCompiler: true
};
